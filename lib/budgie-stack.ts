import * as cdk from '@aws-cdk/core';
import { RestApi, Cors, LambdaIntegration } from '@aws-cdk/aws-apigateway';
import { Table, AttributeType, BillingMode } from '@aws-cdk/aws-dynamodb';
import { Function, Runtime, Code, LayerVersion } from '@aws-cdk/aws-lambda';
import { Rule, Schedule } from '@aws-cdk/aws-events';
import { LambdaFunction } from '@aws-cdk/aws-events-targets';

import credentialsJson from './credentials.json';
type credentialsType = {
  [key: string]: any
}
let credentials: credentialsType = credentialsJson;

const DDB_ACCESS = {
  READ: 'read',
  WRITE: 'write',
  FULL: 'full'
};

type budgieAccessConfiguration = {
  table: Table,
  access: string
}

function createIntegratedFunction(stack: BudgieStack, definition: any) {
  console.log(`creating lambda ${definition.name}...`);
  let lambda = new Function(stack, `${definition.name}-function`, {
    runtime: Runtime.NODEJS_12_X,
    handler: definition.handler,
    code: Code.fromAsset(definition.code),
    environment: definition.environment,
    layers: definition.layers,
    timeout: definition.timeout || cdk.Duration.seconds(5)
  });

  if (!definition.methods && definition.method) {
    definition.methods = [ definition.method ];
  }

  if (!definition.resources && definition.resource) {
    definition.resources = [ definition.resource ];
  }

  let lambdaIntegration = new LambdaIntegration(lambda);
  for (let ri in definition.resources) {
    let resource = definition.resources[ri];
    for (let mi in definition.methods) {
      let method = definition.methods[mi];
      console.log(`adding method ${method} to resource ${resource}...`);
      resource.addMethod(method, lambdaIntegration);
    }
  }

  let ddbAccessConfigurations: budgieAccessConfiguration[] = definition.ddbAccess || [];
  for (let dai in ddbAccessConfigurations) {
    let ddbAccessConfiguration = ddbAccessConfigurations[dai];
    console.log(`granting ${definition.name} ${ddbAccessConfiguration.access} access on ${ddbAccessConfiguration.table.tableName}...`)
    switch (ddbAccessConfiguration.access) {
      case DDB_ACCESS.FULL:
        ddbAccessConfiguration.table.grantFullAccess(lambda);
        break;
      case DDB_ACCESS.READ:
        ddbAccessConfiguration.table.grantReadData(lambda);
        break;
      case DDB_ACCESS.WRITE:
        ddbAccessConfiguration.table.grantWriteData(lambda);
        break;
    }
  }

  let queueAccess = definition.queueAccess || [];
  for (let qai in queueAccess) {
    definition.queueAccess[qai].grantSendMessages(lambda);
  }

  let eventSources = definition.eventSources || [];
  for (let esi in eventSources) {
    lambda.addEventSource(eventSources[esi]);
  }

  if (definition.cron) {
    let rule = new Rule(stack, `${definition.name}-rule`, {
      schedule: definition.cron
    });

    rule.addTarget(new LambdaFunction(lambda));
  }
}

export class BudgieStack extends cdk.Stack {
  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps, corsOrigin?: string) {
    super(scope, id, props);
    const stack = this;
    corsOrigin = corsOrigin || "*";

    // otp table will include
    //  email - string email address
    //  otp - variable size unambiguous characters
    //  purpose
    //  email address
    //  accountType - string eg. owner, moderator, user
    //  TTL on expiration field
    const budgieOtpTable = new Table(stack, 'otp', {
      partitionKey: { name: 'email', type: AttributeType.STRING },
      sortKey: { name: 'otp', type: AttributeType.STRING },
      billingMode: BillingMode.PAY_PER_REQUEST,
      timeToLiveAttribute: 'expiration'
    });

    // user row will include
    //  the user id - generated guid (partition key)
    //  accountType - string eg. owner, moderator, user (sort key)
    //  auth key - generated string
    //  reset key - hash of the user's email address and account id
    //  demographic info - dictionary
    //  account ids - array of guids (user can generate as many as they need)
    //  public account ids - array of accounts that have been made public
    const budgieUsersTable = new Table(stack, 'users', {
      partitionKey: { name: 'userId', type: AttributeType.STRING },
      billingMode: BillingMode.PAY_PER_REQUEST
    });

    // public accounts table
    // pageNumber - partition key (-1 = master page which tracks the other pages)
    // pages - array of pages (available on master page only)
    // accounts - array of string account ids
    const budgiePublicAccountsTable = new Table(stack, 'publicAccounts', {
      partitionKey: { name: 'pageNumber', type: AttributeType.NUMBER },
      billingMode: BillingMode.PAY_PER_REQUEST
    });

    // transactions will include
    //  account id - guid (partition key)
    //  transactionDate - date as number in YYYYMMDD format (sort key)
    //  transactions - array of transaction objects
    const budgieTransactionsTable = new Table(stack, 'transactions', {
      partitionKey: { name: 'accountId', type: AttributeType.STRING },
      sortKey: { name: 'transactionDate', type: AttributeType.NUMBER },
      billingMode: BillingMode.PAY_PER_REQUEST
    });

    // filters are applied when processing new transactions.
    // a filter is applied when it is created. if a filter is updated, then
    //  all the original (replaced) filter's and updated (replacing) filter's
    //  matches are flagged for reprocessing.
    // a filter includes
    //  name - string (partition key)
    //  description - regex string
    //  amount - an expression for amount comparison eg. >=< some value
    //  labels - array of string labels to apply
    const budgieFiltersTable = new Table(stack, 'filters', {
      partitionKey: { name: 'id', type: AttributeType.STRING },
      billingMode: BillingMode.PAY_PER_REQUEST
    });

    // utility layer
    const budgieUtilityLayer = new LayerVersion(stack, 'budgie-utility-layer', {
      // Code.fromAsset must reference the build folder
      code: Code.fromAsset('./layers/build/utility-layer'),
      compatibleRuntimes: [Runtime.NODEJS_12_X],
      license: 'MIT',
      description: 'a layer for providing utility functions',
    });

    // ddb layer
    const budgieDdbLayer = new LayerVersion(stack, 'budgie-ddb-layer', {
      // Code.fromAsset must reference the build folder
      code: Code.fromAsset('./layers/build/ddb-layer'),
      compatibleRuntimes: [Runtime.NODEJS_12_X],
      license: 'MIT',
      description: 'a layer for providing dynamodb table interfaces',
    });

    // otp layer
    const budgieOtpLayer = new LayerVersion(stack, 'budgie-otp-layer', {
      // Code.fromAsset must reference the build folder
      code: Code.fromAsset('./layers/build/otp-layer'),
      compatibleRuntimes: [Runtime.NODEJS_12_X],
      license: 'MIT',
      description: 'a layer for providing OTP / MFA functionality',
    });

    let corsEnvironment = {
      CORS_ORIGIN: corsOrigin
    };

    let registrationEnvironmentBase = {
      USERS_TABLE_NAME: budgieUsersTable.tableName,
      OTP_TABLE_NAME: budgieOtpTable.tableName
    };

    let mailgunEnvironment = {
      MAILGUN_DOMAIN: credentials.mailgun.domain,
      MAILGUN_FROM: credentials.mailgun.from,
      MAILGUN_API_KEY: credentials.mailgun.api_key
    }

    const budgieApi = new RestApi(stack, `budgie-api`, {
      defaultCorsPreflightOptions: {
        allowOrigins: [ corsOrigin ],
        allowMethods: Cors.ALL_METHODS,
      }
    });

    // set up api resources
    const api: any = {
      'root': budgieApi.root
    };

    // /register
    api.register = api.root.addResource('register');

    let registrationFunctions = [
      {
        name: 'user-registration',
        handler: 'index.register',
        code: './handlers/user-registration',
        method: 'POST',
        resource: api.register,
        environment: {
          ...corsEnvironment,
          ...registrationEnvironmentBase,
          ...mailgunEnvironment
        },
        ddbAccess: [
          { table: budgieUsersTable, access: DDB_ACCESS.FULL },
          { table: budgieOtpTable, access: DDB_ACCESS.WRITE }
        ],
        layers: [budgieOtpLayer, budgieDdbLayer, budgieUtilityLayer],
      },
      {
        name: 'user-verification',
        handler: 'index.verify',
        code: './handlers/user-registration',
        method: 'PUT',
        resource: api.register,
        environment: {
          ...corsEnvironment,
          ...registrationEnvironmentBase,
          ...mailgunEnvironment
        },
        ddbAccess: [
          { table: budgieUsersTable, access: DDB_ACCESS.FULL },
          { table: budgieOtpTable, access: DDB_ACCESS.FULL }
        ],
        layers: [budgieOtpLayer, budgieDdbLayer, budgieUtilityLayer],
      },
    ];

    for (let rfi in registrationFunctions) {
      createIntegratedFunction(stack, registrationFunctions[rfi]);
    }

    // all endpoints that follow require a userId in the path
    api.user = api.root.addResource('{userId}');

    // account functions
    // /{userId}/accounts
    api.userAccounts = api.user.addResource('accounts');
    // /{userId}/accounts/{accountId}
    api.userAccount = api.userAccounts.addResource('{accountId}');

    let accountFunctions = [
      {
        name: 'user-account-creation',
        handler: 'index.createAccount',
        code: './handlers/accounts',
        method: 'POST',
        resource: api.userAccounts,
        environment: {
          ...corsEnvironment,
          USERS_TABLE_NAME: budgieUsersTable.tableName
        },
        ddbAccess: [
          { table: budgieUsersTable, access: DDB_ACCESS.FULL }
        ],
        layers: [budgieDdbLayer, budgieUtilityLayer],
      },
      {
        name: 'user-account-retrieval',
        handler: 'index.getAccounts',
        code: './handlers/accounts',
        method: 'GET',
        resource: api.userAccounts,
        environment: {
          ...corsEnvironment,
          USERS_TABLE_NAME: budgieUsersTable.tableName
        },
        ddbAccess: [
          { table: budgieUsersTable, access: DDB_ACCESS.READ }
        ],
        layers: [budgieDdbLayer, budgieUtilityLayer],
      },
      {
        name: 'user-account-privacy',
        handler: 'index.updateAccountPrivacy',
        code: './handlers/accounts',
        method: 'PUT',
        resource: api.userAccount,
        environment: {
          ...corsEnvironment,
          USERS_TABLE_NAME: budgieUsersTable.tableName,
          PUBLIC_ACCOUNTS_TABLE_NAME: budgiePublicAccountsTable.tableName,
        },
        ddbAccess: [
          { table: budgieUsersTable, access: DDB_ACCESS.FULL },
          { table: budgiePublicAccountsTable, access: DDB_ACCESS.FULL }
        ],
        layers: [budgieDdbLayer, budgieUtilityLayer],
      },
    ];

    for (let afi in accountFunctions) {
      createIntegratedFunction(stack, accountFunctions[afi]);
    }

    // account functions
    // /{userId}/transactions
    api.userTransactions = api.user.addResource('transactions');
    // /{userId}/accounts/{accountId}/transactions
    api.userAccountTransactions = api.userAccount.addResource('transactions');
    // /transactions - public transaction endpoint
    api.transactions = api.root.addResource('transactions');

    let transactionFunctions = [
      {
        name: 'transaction-upload',
        handler: 'upload.uploadTransactions',
        code: './handlers/transactions',
        methods: [ 'POST', 'PUT' ],
        resource: api.userTransactions,
        environment: {
          ...corsEnvironment,
          USERS_TABLE_NAME: budgieUsersTable.tableName,
          TRANSACTIONS_TABLE_NAME: budgieTransactionsTable.tableName
        },
        ddbAccess: [
          { table: budgieUsersTable, access: DDB_ACCESS.READ },
          { table: budgieTransactionsTable, access: DDB_ACCESS.FULL }
        ],
        layers: [budgieDdbLayer, budgieUtilityLayer],
        timeout: cdk.Duration.seconds(15)
      },
      {
        name: 'transaction-query',
        handler: 'query.getPrivateTransactions',
        code: './handlers/transactions',
        method: 'GET',
        resources: [ api.userTransactions, api.userAccountTransactions ],
        environment: {
          ...corsEnvironment,
          USERS_TABLE_NAME: budgieUsersTable.tableName,
          TRANSACTIONS_TABLE_NAME: budgieTransactionsTable.tableName
        },
        ddbAccess: [
          { table: budgieUsersTable, access: DDB_ACCESS.READ },
          { table: budgieTransactionsTable, access: DDB_ACCESS.READ }
        ],
        layers: [budgieDdbLayer, budgieUtilityLayer],
      },
      {
        name: 'transaction-delete',
        handler: 'delete.deleteTransactions',
        code: './handlers/transactions',
        method: 'DELETE',
        resources: [ api.userTransactions, api.userAccountTransactions ],
        environment: {
          ...corsEnvironment,
          USERS_TABLE_NAME: budgieUsersTable.tableName,
          TRANSACTIONS_TABLE_NAME: budgieTransactionsTable.tableName
        },
        ddbAccess: [
          { table: budgieUsersTable, access: DDB_ACCESS.READ },
          { table: budgieTransactionsTable, access: DDB_ACCESS.FULL }
        ],
        layers: [budgieDdbLayer, budgieUtilityLayer],
      },
      {
        name: 'transaction-query-public',
        handler: 'query.getPublicTransactions',
        code: './handlers/transactions',
        method: 'GET',
        resource: api.transactions,
        environment: {
          ...corsEnvironment,
          PUBLIC_ACCOUNTS_TABLE_NAME: budgiePublicAccountsTable.tableName,
          TRANSACTIONS_TABLE_NAME: budgieTransactionsTable.tableName
        },
        ddbAccess: [
          { table: budgiePublicAccountsTable, access: DDB_ACCESS.READ },
          { table: budgieTransactionsTable, access: DDB_ACCESS.READ }
        ],
        layers: [budgieDdbLayer, budgieUtilityLayer],
      },
    ];

    for (let tfi in transactionFunctions) {
      createIntegratedFunction(stack, transactionFunctions[tfi]);
    }

  }
}
