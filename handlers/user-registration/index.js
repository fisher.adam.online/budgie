const otp = require('/opt/nodejs/otp-layer/otp');
const users = require('/opt/nodejs/ddb-layer/users');
const utils = require('/opt/nodejs/utility-layer/utils');

const PURPOSE_REGISTRATION = "user registration"

function processEventBody({ body, required }) {
    payload = JSON.parse(body);
    if (!payload) throw new Error("No request body");

    // check for required fields
    let vowels = ['a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U'];
    for (let i in required) {
        let key = required[i];
        if (!payload[key]) {
            let an = vowels.indexOf(key[i].charAt(0)) > -1;
            throw new Error(`Request must include a${an ? 'n' : ''} '${key}' field.`);
        }
    }
    return payload;
}

function sendOTP({ email }) {
    return new Promise((resolve, reject) => {
        otp.createEntry({ email, purpose: PURPOSE_REGISTRATION, accountType: 'user' })
        .then((value) => {
            otp.send({
                email,
                subject: "Budgie: Your One-Time Password",
                body: `Your registration OTP is ${value}.

Please note that as we do not store identifying data, it is possible
to register multiple times using this address. It is strongly
recommended that you do not complete this registration if you already
have a registered user.`
            })
            .then(resolve)
            .catch(reject);
        })
        .catch(reject);
    });
}

// CAUTION: hacky use of otp.send, this is a rush job
function sendUserEmail({ email, userId, authKey }) {
    return new Promise((resolve, reject) => {
        otp.send({
            email,
            subject: "Budgie: Your User Credentials",
            body: `Your User ID is ${userId}, your Authentication Key is ${authKey}.

To submit transactions, update filters or perform queries, use your Authentication Key as a Bearer Token.

Please note that we do not store identifying data. If your Authentican Key is lost or compromised,
it is possible to renew it only if you provide the correct User ID and email address.`
        })
        .then(resolve)
        // on err, resolve with the error as first argument
        .catch(resolve);
    });
}

exports.register = async (event) => {
    return new Promise((resolve, reject) => {
        let payload = null;
        try {
            payload = processEventBody({ body: event.body, required: ['email'] });
        } catch (err) {
            return resolve(utils.formatErrorResponse(400, err));
        }
        let email = payload.email;

        let registrationFailureErrorHandler = (err) => {
            console.error(err);
            resolve(utils.formatErrorResponse(
                500,
                err,
                `Registration of ${email} failed.`
            ));
        };

        // create and send the registration OTP
        sendOTP({ email })
        .then(() => {
            resolve(utils.createResponse({
                "statusCode": 200,
                "body": {
                    "success": true,
                    "message": `Your One-Time Password will be delivered shortly to ${email}.`
                }
            }));
        })
        .catch(registrationFailureErrorHandler);
    });
}

exports.verify = async (event) => {
    return new Promise((resolve, reject) => {
        let payload = null;
        try {
            payload = processEventBody({ body: event.body, required: ['email', 'otp'] });
        } catch (err) {
            return resolve(utils.formatErrorResponse(400, err));
        }

        let email = payload.email;

        let verificationFailureErrorHandler = (err) => {
            console.error(err);
            resolve(utils.formatErrorResponse(
                401,
                err,
                `Verification failed, OTP invalid / expired.`
            ));
        };

        // if otp not found or OTP expiration in the past then return "otp has expired"
        otp.getEntry({ email, otp: payload.otp })
        .then(item => {
            // check otp has the correct purpose
            if (item.purpose != PURPOSE_REGISTRATION) {
                verificationFailureErrorHandler(`otp purpose mismatch: ${item.purpose}`);
            } else {
                let deleteOtp = () => {
                    return new Promise((resolve, reject) => {
                        otp.deleteEntry({
                            email,
                            otp: payload.otp
                        })
                        .then(() => {
                            console.log('otp deleted successfully');
                            resolve();
                        })
                        .catch(err => {
                            console.error('otp deletion failed')
                            console.error(err);
                            resolve();
                        })
                    })
                };

                users.createEntry({ email, accountType: "user" })
                .then(({ userId, authKey}) => {
                    console.log(`user ${userId} entry created successfully for ${email}`);
                    deleteOtp()
                    .then(() => {
                        sendUserEmail({ email, userId, authKey })
                        .then((emailSendError) => {
                            resolve(utils.createResponse({
                                "statusCode": 200,
                                "body": {
                                    "success": true,
                                    "email": emailSendError ? 'not sent' : 'sent',
                                    "message": `Verification succeeded: be sure to store the userId and authKey in a safe place!`,
                                    userId,
                                    authKey
                                }
                            }));
                        });
                    });
                })
                .catch(err => {
                    console.error(`encountered error creating user for ${email}`, err);
                    deleteOtp()
                    .then(() => {
                        resolve(utils.formatErrorResponse(
                            500,
                            err,
                            `User creation failed.`
                        ));
                    });
                });
            }
        })
        .catch(verificationFailureErrorHandler);
    });
}