const moment = require('moment');
const publicAccounts = require('/opt/nodejs/ddb-layer/public-accounts');
const transactions = require('/opt/nodejs/ddb-layer/transactions');
const users = require('/opt/nodejs/ddb-layer/users');
const utils = require('/opt/nodejs/utility-layer/utils');
const uuid = require('uuid').v4;

exports.getPrivateTransactions = async (event) => {
    return new Promise((resolve, reject) => {
        users.authenticate(event)
        .then((user) => {
            // get query parameters from querystring
            let accountId = null;
            if (event.pathParameters && event.pathParameters.accountId) {
                accountId = event.pathParameters.accountId;
            } else {
                if (event.queryStringParameters && event.queryStringParameters.accountId) {
                    accountId = event.queryStringParameters.accountId;
                }
            }

            // validate query parameters
            let startDate, endDate;
            try {
                startDate = event.queryStringParameters.startDate;
                endDate = event.queryStringParameters.endDate;
            } catch (e) {
                console.error(e);
            }
            if (!startDate && !endDate) {
                resolve(utils.formatErrorResponse(
                    400, new Error(`startDate and endDate querystring parameters must be provided`)
                ));
            }

            if (!(
                startDate &&
                endDate &&
                moment(startDate, 'YYYYMMDD', true).isValid() &&
                moment(endDate, 'YYYYMMDD', true).isValid()
                )) {
                resolve(utils.formatErrorResponse(
                    400, new Error(`startDate and endDate must be in the format YYYYMMDD`)
                ));
                return;
            }

            // create array of accountIds to query
            let accountIds = [];
            if (accountId) {
                // single account id specified
                if (user.accounts.indexOf(accountId) < 0) {
                    resolve(utils.formatErrorResponse(
                        401,
                        new Error(`account ${accountId} not found in user accounts`)
                    ));
                    return;
                }
                accountIds.push(accountId);
            } else {
                // all of user's accounts
                accountIds = user.accounts || [];
            }
            // if no accounts available return 404
            if (accountIds.length == 0) {
                resolve(utils.formatErrorResponse(
                    404,
                    null,
                    `No accounts found.`
                ));
                return;
            }

            let promises = [];
            for (var a in accountIds) {
                let accountId = accountIds[a];
                promises.push(transactions.getEntriesInRange({ accountId, startDate, endDate }));
            }

            Promise.all(promises)
            .then(results => {
                // collect results as array of transactions
                let resultArray = [];
                for (let r in results) {
                    let result = results[r];
                    if (!result.success) {
                        console.error(`${result.query} error:`);
                        console.error(result.error);
                        resolve(utils.formatErrorResponse(
                            500,
                            result.error,
                            `Error querying transactions.`
                        ));
                        return;
                    } else {
                        // merge results into array
                        let resultCollection = result.resultCollection;
                        for (let rc in resultCollection) {
                            let transactionMap = resultCollection[rc].transactions;
                            for (let tm in transactionMap) {
                                transactionMap[tm].accountId = resultCollection[rc].accountId;
                                transactionMap[tm].transactionDate = resultCollection[rc].transactionDate.toString();
                                resultArray.push(transactionMap[tm]);
                            }
                        }
                    }
                }

                resolve(utils.createResponse({
                    "statusCode": 200,
                    "body": resultArray
                }));
            })
            .catch((err) => {
                console.error(err);
                resolve(utils.formatErrorResponse(
                    500,
                    err,
                    `Error querying transactions.`
                ));
            });
        })
        .catch((err) => {
            console.error(err);
            resolve(utils.formatErrorResponse(
                401,
                err,
                `Authentication failed, userId / authKey mismatch.`
            ));
        })
   });
}

exports.getPublicTransactions = async (event) => {
    return new Promise((resolve, reject) => {
        // validate query parameters
        let startDate, endDate;
        try {
            startDate = event.queryStringParameters.startDate;
            endDate = event.queryStringParameters.endDate;
        } catch (e) {
            console.error(e);
        }
        if (!startDate && !endDate) {
            resolve(utils.formatErrorResponse(
                400, new Error(`startDate and endDate querystring parameters must be provided`)
            ));
        }

        if (!(
            startDate &&
            endDate &&
            moment(startDate, 'YYYYMMDD', true).isValid() &&
            moment(endDate, 'YYYYMMDD', true).isValid()
            )) {
            resolve(utils.formatErrorResponse(
                400, new Error(`startDate and endDate must be in the format YYYYMMDD`)
            ));
            return;
        }

        // retrieve public accounts
        publicAccounts.getAccounts()
        .then((accountIds) => {
            // retrieve transactions
            let promises = [];
            for (var a in accountIds) {
                let accountId = accountIds[a];
                promises.push(transactions.getEntriesInRange({ accountId, startDate, endDate }));
            }

            // collect and mask transactions
            Promise.all(promises)
            .then(results => {
                // collect results as array of transactions
                let resultArray = [];
                // TODO accountId should be random. uuid?
                for (let r in results) {
                    let accountId = uuid();
                    let result = results[r];
                    if (!result.success) {
                        console.error(`${result.query} error:`);
                        console.error(result.error);
                        resolve(utils.formatErrorResponse(
                            500,
                            result.error,
                            `Error querying transactions.`
                        ));
                        return;
                    } else {
                        // merge results into array
                        let resultCollection = result.resultCollection;
                        for (let rc in resultCollection) {
                            let transactionMap = resultCollection[rc].transactions;
                            for (let tm in transactionMap) {
                                // we only add accountId, date, (masked) description, amount, currency
                                let transaction = transactionMap[tm];
                                resultArray.push({
                                    accountId,
                                    transactionDate: resultCollection[rc].transactionDate.toString(),
                                    description: (transaction.masked ? '***** MASKED *****' : transaction.description),
                                    amount: transaction.amount,
                                    currency: transaction.currency,
                                });
                            }
                        }
                    }
                }

                resolve(utils.createResponse({
                    "statusCode": 200,
                    "body": resultArray
                }));
            })
            .catch((err) => {
                console.error(`error collecting transactions`);
                console.error(err);
                resolve(utils.formatErrorResponse(
                    500,
                    err,
                    `Error querying transactions.`
                ));
            });
        })
        .catch((err) => {
            console.error(`error getting public accounts`);
            console.error(err);
            resolve(utils.formatErrorResponse(
                500,
                err
            ));
        });
    });
};