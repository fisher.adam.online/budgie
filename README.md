# Budgie

Transaction collection, filtering, labelling and querying

Written for the hackathon team #insert-name-here (Adam Fisher, Imraan Parker, Hagashen Naidu)

## Useful commands

* `npm run build`      build layers, compile typescript to js and lint
* `npm run synth`      build and synthesize with cdk
* `cdk deploy [STACK]` deploy the specified stack

## Flow Outline

### Registration

A user registers for an auth key. A registered user can:

* Generate account ids as needed.
* Submit transactions.
* Query transactions.

To register a user, send a POST request to https://host/prod/register with the following JSON payload:

```json
{
  "email": "me@example.com"
}
```

To confirm the user, send a PUT request to https://host/prod/register with the following JSON payload:

```json
{
  "email": "me@example.com",
  "otp": "the OTP received in the registration email"
}
```

Upon successful confirmation, the response will include the `userId` and `authKey`, and these will also be sent to the user's email address.

The response format will be as follows:

```json
{
  "success": true,
  "email": "sent",
  "message": "Verification succeeded: be sure to store the userId and authKey in a safe place!",
  "userId": "c806b683-3180-45ef-9cf3-4440cf792512",
  "authKey": "BjKnZnRLsKeVbqZpE9eYs1cDgW5SWyBiTjdy5orAIan5DMmVSTvKdj7N1C2OCi9s"
}
```

### Account management

An account is simply a GUID that's generated for a specific user and then used to identify transactions.

At present it is the user's responsibility to remember the associations between budgie account IDs with their actual account IDs.

#### Account generation

To generate an account ID, send a POST request to https://host/prod/{userId}/accounts with the `authKey` as a Bearer Token (header: Authorization=Bearer *authKey*). Any payload will be ignored.

The response format will be as follows:

```json
{
  "success": true,
  "message": "Your new account ID is c64b178a-5bd7-421f-8628-9ae973a36bc9."
}
```

#### Retrieving accounts

To retrieve a list of generated account IDs, send a GET request to https://host/prod/{userId}/accounts with the `authKey` as a Bearer Token (header: Authorization=Bearer *authKey*) and no payload (a GET request with a payload will be blocked by the API Gateway).

#### Making an account public or private

An account is private by default, meaning that its transactions are not available for public consumption. When an account is made public, limited transaction data will be made available for public consumption. See [Querying section](#querying) below for more details.

To update an account's privacy status, send a PUT request to https://host/prod/{userId}/accounts/{accountId} with the `authKey` as a Bearer Token (header: Authorization=Bearer *authKey*) and a payload indicating the desired privacy status as follows:

```json
{
    "accountStatus": "public" // or "private"
}
```

### Uploading Transactions

A user submits transactions as a `POST` or `PUT` request to https://host/prod/{userId}/transactions with the `authKey` as a Bearer Token (header: Authorization=Bearer *authKey*) and the payload in the following format:

```json
{
  "ACCOUNT_ID_1": [ // account id is the key
    {
      "transactionId": "TRANSACTION_ID_1", // string, uniqueness per account per day is uploader's responsibility
      "transactionDate": "20201225", // string in the format YYYYMMDD
      "description": "string",
      "amount": "1099", // integer value of smallest denomination
      "currency": "ZAR",
      "labels": [ "string", "array" ], // any labels that will be applied regardless of processing
      "masked": false // true if description must be obscured for public download
    },
    ...
  ],
  "ACCOUNT_ID_2": [ // transactions for multiple accounts can be uploaded simultaneously
    ...
}
```

The response format will be as follows:

```json
{
  "success": false, // true only if every transaction uploaded successfully
  "results": [
    {
      // on validation failure, no transactions for the account will be submitted
      "accountId": "ACCOUNT_ID_1",
      "validationErrors": [
        {
          "transactionId": "TRANSACTION_ID_1",
          "description": "TRANSACTION_DESCRIPTION",
          "errors": [
              "transaction date missing",
              "amount invalid"
          ]
        }
      ],
      "successes": 0, // successes for account batch
      "failures": 1 // failures for account batch
    },
    {
      // if any transaction uploads fail, failed transaction Ids returned
      "accountId": "ACCOUNT_ID_2",
      "failedTransactionDates": [ "20201225" ],
      "successes": 1, // successes for account batch
      "failures": 1 // failures for account batch
    },
    // transactions uploaded successfully
    {
      "accountId": "ACCOUNT_ID_3",
      "successes": 1, // successes for account batch
      "failures": 0 // failures for account batch
    },
    ...
  ],
  "successes": 2, // total successes
  "failures": 2 // total failures
}
```

### Deleting Transactions

A user can query their private transactions by accountId and date range.

A user submits a `DELETE` request to https://host/prod/{userId}/transactions (for all the user's accounts) or https://host/prod/{userId}/account/{accountId}/transactions (for the specified account) with the `authKey` as a Bearer Token (header: Authorization=Bearer *authKey*) and no payload (a DELETE request with a payload will be blocked by the API Gateway).

The response format will be as follows:

```json
{
  "success": true // or false with a reason and error object
}
```

### Querying

#### Personal transactions

A user can query their private transactions by accountId and date range.

A user submits a query as a `GET` request to https://host/prod/{userId}/transactions (for all the user's accounts) or https://host/prod/{userId}/account/{accountId}/transactions (for the specified account) with the `authKey` as a Bearer Token (header: Authorization=Bearer *authKey*) and no payload (a GET request with a payload will be blocked by the API Gateway).

The query string must include two parameters: `startDate` and `endDate` in the format `YYYYMMDD`.

```json
[
    {
        "description": "transaction description",
        "amount": 1234,
        "currency": "ZAR",
        "transactionId": "unique string per transaction per date",
        "masked": false,
        "labels": [
            "string label"
        ],
        "accountId": "string",
        "transactionDate": "20200131"
    },
    ...
]
```

#### Public transactions

Anyone can retrieve public transactions for a given date range.

A client submits a query as a `GET` request to https://host/prod/transactions with no payload (a GET request with a payload will be blocked by the API Gateway).

The query string must include two parameters: `startDate` and `endDate` in the format `YYYYMMDD`.

The transactions will be retrieved from accounts selected at random from a pool of public accounts. These accounts will be assigned GUIDs that are generated for each query.

Transaction descriptions will be returned for transactions that have not been explicitly masked (see the [Transaction Upload section](#transaction-upload)). At present, labels will not be returned.

```json
[
    {
        "accountId": "ffbb4b04-6c14-4289-9d1f-ff1f168ef90d",
        "transactionDate": "20201111",
        "description": "transaction description",
        "amount": 1234,
        "currency": "ZAR"
    },
    {
        "accountId": "ffbb4b04-6c14-4289-9d1f-ff1f168ef90d",
        "transactionDate": "20201111",
        "description": "***** MASKED *****",
        "amount": 2500,
        "currency": "ZAR"
    }
]
```
