const aws = require('aws-sdk');
const dynamodb = new aws.DynamoDB.DocumentClient();
const publicAccounts = require('/opt/nodejs/ddb-layer/public-accounts');
const sfet = require('simple-free-encryption-tool');
const uuid = require('uuid').v4;

const USERS_TABLE = process.env.USERS_TABLE_NAME;

let self = {
    createEntry: ({ email, accountType }) => {
        return new Promise((resolve, reject) => {
            let userId = uuid();
            let authKey = sfet.utils.randomstring.generate(64);
            let hashedAuthKey = sfet.sha256.hash(authKey);
            let resetKey = sfet.sha256.hash(userId + email);
            dynamodb.put({
                TableName: USERS_TABLE,
                Item: {
                    userId,
                    accountType,
                    authKey: hashedAuthKey,
                    resetKey
                }
            }).promise()
            .then(() => { resolve({ userId, authKey }); })
            .catch(reject);
        });
    },
    authenticate: (event) => {
        return new Promise((resolve, reject) => {
            let userId = event.pathParameters.userId;
            console.log(`authenticate called for user ${userId}`);

            // get Bearer token from Authorization header
            let authKey = event.headers.Authorization;
            if (!authKey) {
                return reject(new Error('Missing Authorization header'));
            }
            if (authKey.indexOf('Bearer ') != 0) {
                console.error(`Authorization header format invalid`);
                return reject(new Error('Invalid Authorization header'));
            }
            authKey = authKey.substring(7);
            let hashedAuthKey = sfet.sha256.hash(authKey);

            self.getEntry({ userId })
            .then((user) => {
                if (user.authKey != hashedAuthKey) {
                    console.error(`authKey hash mismatch`);
                    reject(new Error('Invalid Authorization header'));
                } else {
                    resolve(user);
                }
            })
            .catch(reject);
        });
    },
    getEntry: ({ userId }) => {
        return new Promise((resolve, reject) => {
            console.log(`users.getEntry called for user ${userId}`);
            dynamodb.get({
                TableName: USERS_TABLE,
                Key: {
                    "userId": userId
                }
            }).promise()
            .then(result => { resolve(result.Item); })
            .catch(reject);
        });
    },
    createAccount: ({ user }) => {
        return new Promise((resolve, reject) => {
            user.accounts = user.accounts || [];
            let newAccountId = uuid();
            user.accounts.push(newAccountId);

            console.log(`createAccount updating user entry ${user.userId} with account ${newAccountId}`);
            dynamodb.update({
                TableName: USERS_TABLE,
                Key: {
                    "userId": user.userId
                },
                UpdateExpression: "set accounts = :accounts",
                ExpressionAttributeValues:{
                    ":accounts": user.accounts
                }
            }).promise()
            .then(() => { resolve(newAccountId); })
            .catch(reject);
        });
    },
    updateAccountPrivacy: ({ user, accountId, isPrivacyToBeEnabled }) => {
        return new Promise((resolve, reject) => {
            console.log(`users.updateAccountPrivacy updating user ${user.userId} account ${accountId} privacy status to ${isPrivacyToBeEnabled}`);
            user.publicAccounts = user.publicAccounts || [];
            if (isPrivacyToBeEnabled) {
                // first update the public account tables, if we fail
                // to update the user entry afterwards that's less of
                // a problem than the other way around
                publicAccounts.removeEntry({ accountId })
                .then(()=>{
                    // remove the entry from the user's publicAccounts
                    let index = user.publicAccounts.indexOf(accountId);
                    if (index < 0) {
                        console.log(`accountId ${accountId} already private in user object`);
                        resolve();
                        return;
                    }
                    user.publicAccounts.splice(index, 1);
                    dynamodb.update({
                        TableName: USERS_TABLE,
                        Key: {
                            "userId": user.userId
                        },
                        UpdateExpression: "set publicAccounts = :publicAccounts",
                        ExpressionAttributeValues:{
                            ":publicAccounts": user.publicAccounts
                        }
                    }).promise()
                    .then(resolve)
                    .catch(reject);
                })
                .catch(reject);
            } else {
                // first update the user, if we fail to make the
                // account public afterwards that's less of a problem
                // than the other way around
                if (user.publicAccounts.indexOf(accountId) < 0) {
                    user.publicAccounts.push(accountId);
                    dynamodb.update({
                        TableName: USERS_TABLE,
                        Key: {
                            "userId": user.userId
                        },
                        UpdateExpression: "set publicAccounts = :publicAccounts",
                        ExpressionAttributeValues:{
                            ":publicAccounts": user.publicAccounts
                        }
                    }).promise()
                    .then(() => {
                        publicAccounts.addEntry({ accountId })
                        .then(resolve)
                        .catch(reject);
                    })
                    .catch(reject);
                } else {
                    console.log(`accountId already public for user ${user.userId}`);
                    publicAccounts.addEntry({ accountId })
                    .then(resolve)
                    .catch(reject);
                }
            }
        });
    },
    removeAccount: ({ userId, accountId }) => {},
    // allow partial updates only, to remove something null must be specified
    updateDemographics: ({ userId, demographics }) => {
        return new Promise((resolve, reject) => {
            dynamodb.update({
                TableName: USERS_TABLE,
                Key: {
                    "userId": userId
                },
                UpdateExpression: "set active = :a",
                ExpressionAttributeValues:{
                    ":a": active
                }
            }).promise()
            .then(result => { resolve(result.Item); })
            .catch(reject);
        });
    }
};

module.exports = self;